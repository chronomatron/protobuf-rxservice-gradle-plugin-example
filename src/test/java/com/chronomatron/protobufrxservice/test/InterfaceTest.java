package com.chronomatron.protobufrxservice.test;


import org.junit.Test;
import rx.Observable;

import java.lang.reflect.Proxy;

import static org.junit.Assert.assertNotNull;

public class InterfaceTest {
    @Test
    public void testInterfaceCompilation() {
        TestProto.SearchService service = createService();
        Observable<TestProto.SearchResponse> response = service.Search(null);
        assertNotNull(response);
    }

    private TestProto.SearchService createService() {
        return (TestProto.SearchService) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                new Class[]{TestProto.SearchService.class}, (proxy, method, args) -> Observable.empty());
    }
}